#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include "parser.h"
#include "utils.h"
#include "main.h"
#include "generator.h"
#include "djikstra.h"


Network *n;
Warehouse **path;
int *lengthPath;


void welcome() {
	printf(" WELCOME IN '%s' %s\n\n", NAME_PGM, VERSION);
	printf("A network has been created! You have now the possibility\nto add some warehouses (nodes) to this network.\n");
}

void display_menu() {
	printf(" %s - Create a new warehouse (%s)\n", NUM_NEW, CMD_NEW);
	printf(" %s - List of warehouses (%s)\n", NUM_LIST, CMD_LIST);
	printf(" %s - Print informations about a warehouse (%s)\n", NUM_DESC, CMD_DESC);
	printf(" %s - Create a link between warehouses (%s)\n", NUM_LINK, CMD_LINK);
	printf(" %s - Launch Djikstra pathfinding (%s)\n", NUM_DJIK, CMD_DJIK);
	printf(" %s - Generate random network (%s)\n", NUM_RAND, CMD_RAND);
	printf(" %s - Load an existing DOT file (%s)\n", NUM_LOAD, CMD_LOAD);
	printf(" %s - Save the current network in a file (%s)\n", NUM_SAVE, CMD_SAVE);
	printf(" %s - Exit the pr0gramm (%s)\n\n", NUM_EXIT, CMD_EXIT);
}


void get_input_menu(Network *n) {
	char cmd[SIZE_CMD + 2] = "";
	
	while(strcmp(cmd, CMD_EXIT) != 0) {
		printf("\n");
		display_menu();

		printf("> ");
		fgets(cmd, SIZE_CMD, stdin);
		remove_newline(cmd);
		to_upper_case(cmd);
		//clear_terminal();

		if(strcmp(cmd, CMD_NEW) == 0 || strcmp(cmd, NUM_NEW) == 0)
			ask_new_warehouse(n);
	
		if(strcmp(cmd, CMD_LIST) == 0 || strcmp(cmd, NUM_LIST) == 0)
			print_network(n);		

		if(strcmp(cmd, CMD_DESC) == 0 || strcmp(cmd, NUM_DESC) == 0)
			print_warehouse(ask_get_warehouse(n, NULL));

		if(strcmp(cmd, CMD_LINK) == 0 || strcmp(cmd, NUM_LINK) == 0)
			ask_create_link(n);

		if(strcmp(cmd, CMD_DJIK) == 0 || strcmp(cmd, NUM_DJIK) == 0)
			path = ask_djikstra(n, lengthPath);

		if(strcmp(cmd, CMD_LOAD) == 0 || strcmp(cmd, NUM_LOAD) == 0)
			load_graph(n);

		if(strcmp(cmd, CMD_RAND) == 0 || strcmp(cmd, NUM_RAND) == 0)
			ask_generate_random_network(n);

		if(strcmp(cmd, CMD_SAVE) == 0 || strcmp(cmd, NUM_SAVE) == 0)
			save_graph(n, path, *lengthPath);
		
		if(strcmp(cmd, CMD_EXIT) == 0 || strcmp(cmd, NUM_EXIT) == 0)
			exit_programm();
	}
}


Warehouse* ask_get_warehouse(Network *n, char *instruction) {
	char name[SIZE_NAME + 2];
	if(instruction == NULL) {
		instruction = "Name of the warehouse";
	}
	Warehouse *tmp;
	int valid = 0;
	printf(" %s: ", instruction);
	fgets(name, SIZE_NAME, stdin);
	remove_newline(name);
	tmp = get_warehouse(n, name);
	valid = tmp != NULL;
	
	if(valid == 0)
		printf(" -> Unknown warehouse '%s'\n", name);

	return tmp;
}


void define_network(Network *n) {
	if(n != NULL) {
		if(strcmp(n->name, "") == 0) {
			char name[SIZE_NAME + 2];
			printf("Before adding somes warehouses, you have to define your network.\nName of the network: ");
			fgets(name, SIZE_NAME, stdin);
			remove_newline(name);
			set_name(n, name);
		}
	}
}


void ask_new_warehouse(Network *n) {
	define_network(n);
	char name[SIZE_NAME];
	printf(" == New warehouse ==\n");
	printf(" name: ");
	fgets(name, SIZE_NAME, stdin);
	remove_newline(name);

	if(get_warehouse(n, name) == NULL) {
		add_node(n, name);
		printf("\n == Warehouse '%s' added!\n\n", name);
	}
	else {
		printf("\n -> Warehouse '%s' already exists!", name);	
	}
}


void ask_create_link(Network *n) {
	Warehouse *src = NULL;
	Warehouse *dest = NULL;
	int distance;

	printf("== New link ==\n");	

	src = ask_get_warehouse(n, "name of the source warehouse");
	if(src != NULL)
		dest = ask_get_warehouse(n, "name of the destination warehouse");
	if(dest != NULL) {
		printf(" Distance: ");
		scanf("%d", &distance);
	}
	
	if(dest != NULL)
		add_link(src, dest, distance);
}


Warehouse** ask_djikstra(Network *n, int *lengthPath) {
	if(strcmp(n->name, "") != 0) {
		Warehouse *src = NULL;
		Warehouse *dest = NULL;
		printf("== Djikstra pathfinding ==\n");	
		src = ask_get_warehouse(n, "name of the source warehouse");
		if(src != NULL)
			dest = ask_get_warehouse(n, "name of the destination warehouse");
		
		if(src != NULL && dest != NULL)
			return djikstra(n, src, dest, lengthPath);
		else 
			return NULL;
	}
	else {
		printf("-> There is not network loaded !\n");	
		return NULL;
	}
}


void ask_generate_random_network(Network *n) {
	define_network(n);
	int nbNodes;
	double averageNmbrLinks, standardDeviationNmbrLinks, averageLengthsLinks, standardDeviationLengthsLinks;

	printf("Number of nodes : ");
	scanf("%d", &nbNodes);

	printf("Average of the number of links : ");
	scanf("%lf", &averageNmbrLinks);

	printf("Standard deviation of the number of links : ");
	scanf("%lf", &standardDeviationNmbrLinks);

	printf("Average of the lengths of the links : ");
	scanf("%lf", &averageLengthsLinks);

	printf("Standard deviation of the lengths of the links : ");
	scanf("%lf", &standardDeviationLengthsLinks);
	
	generate_random_network(n, nbNodes, averageNmbrLinks, standardDeviationNmbrLinks, averageLengthsLinks, standardDeviationLengthsLinks);
}


void load_graph(Network *n) {	
	char filename[SIZE_CMD + 2];
	if(strcmp(n->name, "") != 0 && n->hasChanged == 1) {
		printf("Do you want to save your current network? [Y|N] : ");
		fgets(filename, SIZE_CMD, stdin);
		printf("%s\n", filename);
		if(strcmp(filename, "Y") == 0||strcmp(filename, "y") == 0)
			save_graph(n, path, *lengthPath);
	}

	printf("filename: ");
	fgets(filename, SIZE_CMD, stdin);
	remove_newline(filename);
	Parser *p = new_parser(filename);;
	if(p != NULL) {
		load(p, n);
		if(p->error == 0)
			printf(" == File '%s' loaded with success!\n", filename);
		else {
			printf("-> The DOT file '%s' is incorrect!\n\n", filename);
			set_name(n, "");
		}
	}
	else
		printf("-> Cannot load '%s'!\nPlease check if it exist\n", filename); 

	free_parser(p);
	n->hasChanged = 0;
}


void save_graph(Network *n, Warehouse **path, int lengthPath) {
	define_network(n);

	if(n->hasChanged == 1) {
		char *f;
		if(path == NULL) {
			f = save(n);
			printf(" == '%s' saved! In order to generate an image file, execute:\n\n\t./create_image.sh %s\n", f, f);
		}
		else {
			f = saveWithPath(n, path, lengthPath);
			printf(" == '%s' saved! In order to generate an image file with the path to follow, execute:\n\n\t./create_image.sh %s\n", f, f);
		}
					
		free(f);
	}
}


void exit_programm() {
	if(strcmp(n->name, "") != 0)
		save_graph(n, path, *lengthPath);
	printf("\n Bye bye!\n");
	free_network(n);
	free(path);
	free(lengthPath);

	exit(EXIT_SUCCESS);
}


int main(int argc, char const *argv[]) {
	n = new_network(NULL);
	lengthPath = (int*) malloc(sizeof(int));
	signal(SIGINT, exit_programm);
	srand(time(NULL));
	welcome();
	get_input_menu(n);
	return EXIT_SUCCESS;
}