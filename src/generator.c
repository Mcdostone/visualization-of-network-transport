#include <stdio.h>
#include <stdlib.h>
#include "generator.h"
#include "utils.h"

#define COLOR "red"


char* save(Network *n) {
	char *withExt = (char *) malloc(SIZE_NAME * sizeof(char));

	if(n != NULL && n->hasChanged == 1) {
		char *filename = sanityze(n->name);
		strcpy(withExt, filename);
		strcat(withExt, ".dot");
		FILE *dot = fopen(withExt, "wr+");
   	
	   	fprintf(dot, "digraph %s {\n\n", filename);
		Warehouse *current = NULL;

		for (int i = 0; i < n->nbNodes; i++) {
			current = n->nodes[i];
			
			if(current->nbLinks == 0)
				fprintf(dot, "\t\"%s\"\n",current->name);
			else {
				for(int j = 0; j < current->nbLinks; j++)
					fprintf(dot, "\t\"%s\" -> \"%s\" [ label = \"%d\" ]\n", current->name, current->links[j]->name, current->distances[j]);
			}
			
		}
		
		fprintf(dot, "\n}\n");
		fclose(dot);
		n->hasChanged = 0; 
		free(filename);
	}
	
	return withExt;
}


Warehouse* findWarehouse(Warehouse **w, Warehouse *ref, int lengthPath) {
	Warehouse *tmp = *w;	
	int i = 0;

	while(tmp != ref && i < lengthPath) {
		i = i + 1;
		tmp = *(w + i);
	}
	if(tmp == ref)
		return (i <= lengthPath - 1) ? *(w + i + 1) : NULL;
	else
		return NULL;
}



char* saveWithPath(Network *n, Warehouse **w, int lengthPath) {
	char *withExt = (char *) malloc(SIZE_NAME * sizeof(char));	
	if(n != NULL && n->hasChanged == 1) {
		char *filename = sanityze(n->name);
		strcpy(withExt, filename);
		strcat(withExt, ".dot");
		FILE *dot = fopen(withExt, "wr+");;

	   	fprintf(dot, "digraph %s {\n\n", filename);
		Warehouse *current;
		for (int i = 0; i < n->nbNodes; i++) {
			current = n->nodes[i];
			Warehouse *next = findWarehouse(w, current, lengthPath);			
			
			if(current->nbLinks == 0)
				fprintf(dot, "\t\"%s\"\n",current->name);
			else {
				for(int j = 0; j < current->nbLinks; j++) {
					if(current->links[j] == next)
						fprintf(dot, "\t\"%s\" -> \"%s\" [ label = \"%d\" , color = \"%s\" ]\n", current->name, current->links[j]->name, current->distances[j], COLOR);
					else
						fprintf(dot, "\t\"%s\" -> \"%s\" [ label = \"%d\" ]\n", current->name, current->links[j]->name, current->distances[j]);
				}
			}
			
		}
		
		fprintf(dot, "\n}\n");
		fclose(dot);
		n->hasChanged = 0; 

		free(filename);
	}
	
	return withExt;
}