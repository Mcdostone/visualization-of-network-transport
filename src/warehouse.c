#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "warehouse.h"


Warehouse* new_warehouse(char *name) {
	Warehouse* ptr;
	ptr = (Warehouse *) malloc(sizeof(Warehouse));
	if(ptr == NULL)
		return NULL;

	strcpy(ptr->name, name);
	ptr->nbLinks = 0;
	
	return ptr;
}


void free_warehouse(Warehouse *w) {
	if(w != NULL)
		free(w->name);	
}


void add_link(Warehouse *src, Warehouse *dest, int distance) {
	if(src != NULL && dest != NULL) {
		src->links[src->nbLinks] = dest;
		src->distances[src->nbLinks] = distance;
		src->nbLinks = src->nbLinks + 1;
	}
}


void print_warehouse(Warehouse *w) {
	if(w != NULL) {
		int width;
		width = 18;
		char* border = "------------------------";
		
		printf("%.*s\n",width, border);
		printf("|  %-12.12s  |\n", w->name);
		printf("%.*s\n",width, border);
		if(w->nbLinks > 0) {
			printf("| %-4d %-9s |\n", w->nbLinks, "links:");
			for (int i = 0; i < w->nbLinks; i++)
				printf("| - %-7.7s, %-3d |\n",w->links[i]->name, w->distances[i]);
			
			printf("%.*s\n",width, border);
		}
	}
}