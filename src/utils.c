#include <stdlib.h>
#include <stdio.h>
#include <utils.h>
#include <ctype.h>
#include <math.h>
#include "definitions.h"


void to_upper_case(char *string) {
	for (int i = 0; i < strlen(string); i++)
		string[i] = toupper(string[i]);
}


void remove_newline(char *string) {
	string[strlen(string) - 1] = '\0';
}


void clear_terminal(void) {
	printf("\033c");
}


char* sanityze(char *string) {	
	char *sanityzed = (char *) malloc(SIZE_NAME * sizeof(char));
	strcpy(sanityzed, string);
	
	if(sanityzed != NULL) {
		for (int i = 0; i < strlen(sanityzed); i++) {
			if(*(sanityzed + i) == ' ')
				*(sanityzed + i) = '_';
		}
	}
	else
		sanityzed = "my_file";

	return sanityzed;
}


int* uniqueSetRandomNumber(int nbRandomNumbers, int minRange, int maxRange, int avoidedNumber){
	if(minRange <= maxRange && nbRandomNumbers <= maxRange + 1 - minRange && minRange <= avoidedNumber && avoidedNumber <= maxRange) {
		int* selectedNumbers = malloc(sizeof(int)*nbRandomNumbers);
		int numbersArray[maxRange - minRange];
		int i = 0, c = minRange;

		for (i = 0; i < maxRange - minRange; i++){
			if(c == avoidedNumber)
				c++;
			numbersArray[i] = c;
			c++;
		}

		int remainingRandomNumber = maxRange + 1 - minRange - 1;

		for(i = 0; i < nbRandomNumbers; i++){
			int n = rand() % remainingRandomNumber;
			*(selectedNumbers + i) = numbersArray[n];
			numbersArray[n] = numbersArray[remainingRandomNumber - 1];
			remainingRandomNumber = remainingRandomNumber - 1;
		}

		return selectedNumbers;
	}

	return NULL;
}

double getTriangularDistributionSample(double expectedValue, double standardDeviation){
	double y = (double)rand() / (double)RAND_MAX;
	double a = expectedValue;
	double b = standardDeviation / sqrt(6);
	if(y <= 0.5)
		return b * sqrt(2*y) + a - b;
	else
		return b * (1 - sqrt(2 * (1 - y))) + a;
}

char* remove_double_quotes(char *s) {
	char *cleaned;
	cleaned = (char *) malloc((strlen(s) - 1) * sizeof(char));
	*(cleaned + strlen(s) -2) = '\0';
	strncpy(cleaned, s + 1, strlen(s) - 2);

	return cleaned;
}