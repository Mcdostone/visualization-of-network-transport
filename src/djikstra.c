#include <stdio.h>
#include <stdlib.h>
#include "djikstra.h"


Warehouse** djikstra(Network *n, Warehouse *start, Warehouse *destination, int *pathLength){
	printf("\n == Resuts of Djikstra ==\n");

	// Declare iterators
	int i;
	
	// Initialize processed warehouses array ; warehouses which are reachable 
	Warehouse *warehouses[n->nbNodes];

	// For each warehouse, store the index of the warehouse which leads to the shortest path
	//Warehouse* warehouseBestPath[n->nbNodes];
	Warehouse **warehouseBestPath = (Warehouse**) (malloc(sizeof(Warehouse*) * n->nbNodes));

	for (i = 0; i < n->nbNodes; i++) {
		warehouseBestPath[i] = NULL;
		warehouses[i] = NULL;
	}
	
	warehouseBestPath[0] = start;

	// Iterator to remember which warehouses have been processed or need to be processed
	int toBeProcessedWarehousesIndex = 0, processedWarehousesIndex = 0;

	// Add the start warehouse(s) to the array of processed warehouses
//	printf("\nAjout de %s à la liste des warehouses\n", start->name);
	warehouses[0] = start;
	toBeProcessedWarehousesIndex++;

	// Initialize potential array
	int potentials[n->nbNodes];
	for(i=0;i<n->nbNodes;i++){
		potentials[i] = 0;
	}


	// Process each warehouses
	int currentWarehouseIndex = 0;
	int linkedWarehouseIndex;
	// If the destination warehouse is reached or if there's no more warehouses to be processed the loop is stopped
	while(warehouses[currentWarehouseIndex] != destination && processedWarehousesIndex != toBeProcessedWarehousesIndex){
//		printf("\nTraitement de %s\n", warehouses[currentWarehouseIndex]->name);
		// Iterate throught all the linked warehouses
		for(i=0;i<warehouses[currentWarehouseIndex]->nbLinks;i++){
			// If the linked warehouse has not already been processed
			if(!isWarehouseUnderThreshold(warehouses[currentWarehouseIndex]->links[i], processedWarehousesIndex, warehouses)){
//				printf("---Lien de %s vers %s\n", warehouses[currentWarehouseIndex]->name, warehouses[currentWarehouseIndex]->links[i]->name);
				
				// If the linked warehouse hasn't already been met then add it to the unprocessed warehouses
				if(!isWarehouseUnderThreshold(warehouses[currentWarehouseIndex]->links[i], toBeProcessedWarehousesIndex, warehouses)){
//					printf("------Ajout de %s à la liste des warehouses\n", warehouses[currentWarehouseIndex]->links[i]->name);
					warehouses[toBeProcessedWarehousesIndex] = warehouses[currentWarehouseIndex]->links[i];
					toBeProcessedWarehousesIndex++;
				}

				linkedWarehouseIndex = getWarehouseIndex(warehouses[currentWarehouseIndex]->links[i], toBeProcessedWarehousesIndex, warehouses);
				
//				printf("------Potentiel actuel : %d / Potentiel calcule : %d\n", potentials[linkedWarehouseIndex], potentials[currentWarehouseIndex] + warehouses[currentWarehouseIndex]->distances[i]);
				// If the new potential calculated is less than the old one, it is replaced
				if(potentials[linkedWarehouseIndex] > potentials[currentWarehouseIndex] + warehouses[currentWarehouseIndex]->distances[i] || potentials[linkedWarehouseIndex] == 0){
					potentials[linkedWarehouseIndex] = potentials[currentWarehouseIndex] + warehouses[currentWarehouseIndex]->distances[i];
					warehouseBestPath[linkedWarehouseIndex] = warehouses[currentWarehouseIndex];
				}
			}
		}

		processedWarehousesIndex++;

		// Get the index of the next warehouse to be processed meaning the one with the less potential
		currentWarehouseIndex = getLessPotentialWarehouse(processedWarehousesIndex, toBeProcessedWarehousesIndex, potentials, warehouses);
		
		Warehouse* temp1 = warehouseBestPath[processedWarehousesIndex];
		warehouseBestPath[processedWarehousesIndex] = warehouseBestPath[currentWarehouseIndex];
		warehouseBestPath[currentWarehouseIndex] = temp1;

		int temp2 = potentials[processedWarehousesIndex];
		potentials[processedWarehousesIndex] = potentials[currentWarehouseIndex];
		potentials[currentWarehouseIndex] = temp2;

		Warehouse* temp3 = warehouses[processedWarehousesIndex];
		warehouses[processedWarehousesIndex] = warehouses[currentWarehouseIndex];
		warehouses[currentWarehouseIndex] = temp3;

		currentWarehouseIndex = processedWarehousesIndex;
		n->hasChanged = 1;
	}

	if(warehouses[currentWarehouseIndex] == destination){
		//printf("\n=== Affichage des potentiels ===\n\n");
		for(i=0;i<processedWarehousesIndex;i++)
			printf("%s : %d\n", warehouses[i]->name, potentials[i]);

		//printf("\n=== Affichage du chemin le plus court ===\n\n");
		int destinationId = getWarehouseIndex(destination, processedWarehousesIndex + 1, warehouses);
		Warehouse** path = getPath(destinationId, processedWarehousesIndex + 1, warehouses, warehouseBestPath, pathLength);
		//printf("Distance %d en %d noeuds\n", potentials[destinationId], *pathLength);

		free(warehouseBestPath);

		printf("%s", (*path)->name);
		for(i=1;i<*pathLength;i++)
			printf(" => %s", (*(path+i))->name);
		printf("\n");
		return path;
	}
	else{
		printf("There is no path!\n\n");
		free(warehouseBestPath);
		return NULL;
	}
}


Warehouse** getPath(int destinationId, int maxIndexThreshold, Warehouse *warehouses[], Warehouse* warehouseBestPath[], int* pathLength){
	int curWarehouseIndex = destinationId;
	Warehouse* pathTemp[maxIndexThreshold];
	int i = maxIndexThreshold - 1;
	while(i > 0 && warehouses[curWarehouseIndex] != warehouseBestPath[curWarehouseIndex]){
		pathTemp[i] = warehouses[curWarehouseIndex];
		curWarehouseIndex = getWarehouseIndex(warehouseBestPath[curWarehouseIndex], maxIndexThreshold, warehouses);
		i--;
	}
	pathTemp[i] = warehouses[curWarehouseIndex];
	*pathLength = maxIndexThreshold - i;
	Warehouse** path = (Warehouse**) malloc(sizeof(Warehouse*) * (*pathLength));
	for(i=0;i<*pathLength;i++)
		path[i] = pathTemp[maxIndexThreshold - *pathLength + i];
	return path;
}


// Check if a warehouse isn't already processed
int isWarehouseUnderThreshold(Warehouse *w, int maxIndexThreshold, Warehouse *warehouses[]){
	return getWarehouseIndex(w, maxIndexThreshold, warehouses) == -1 ? 0 : 1;
}


// Return the index of the warehouse and of its potential
int getWarehouseIndex(Warehouse *w, int maxIndexThreshold, Warehouse *warehouses[]){
	int i;
	for(i=0;i<maxIndexThreshold;i++)
		if(warehouses[i] == w)
			return i;
	return -1;
}


int getLessPotentialWarehouse(int minIndexThreshold, int maxIndexThreshold, int potentials[], Warehouse *warehouses[]){
	int lessPotentialWarehouseIndex = minIndexThreshold;
	int i;
	for(i=minIndexThreshold+1;i<maxIndexThreshold;i++)
		if(potentials[i] < potentials[lessPotentialWarehouseIndex])
			lessPotentialWarehouseIndex = i;
	return lessPotentialWarehouseIndex;
}