#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "network.h"
#include "utils.h"


Network* new_network(char *name) {
	Network* ptr;
	ptr = (Network *) malloc(sizeof(Network));
	if(ptr == NULL)
		return NULL;
	
	ptr->hasChanged = 0;
	
	if(name != NULL) {
		strcpy(ptr->name, name);
		ptr->hasChanged = 1;
	}	
	else
		strcpy(ptr->name, "");
	
	ptr->nbNodes = 0;
	
	return ptr;
}


void free_network(Network *n) {
	if(n != NULL) {
		for (int i = 0; i < n->nbNodes; i++)
			free_warehouse(n->nodes[i]);
		free(n);
	}
}


void set_name(Network *n, char *name) { 
	if(name != NULL && n != NULL) {
		strcpy(n->name, name);
		n->hasChanged = 1;
	}
}


Warehouse* add_node(Network *n, char *name) {
	Warehouse *w;
	if(n != NULL && name != NULL) {
		// We can't add two warehouses with the same name
		w = get_warehouse(n, name);
		if(w == NULL) {
			w = new_warehouse(name);
			n->nodes[n->nbNodes] = w;
			n->nbNodes = n->nbNodes + 1;
			n->hasChanged = 1;
		}
	
		return w;
	}
	
	return NULL;
}


Warehouse* get_warehouse(Network *n, char *name) {
	for (int i = 0; i < n->nbNodes; i++) {
		if(strcmp(n->nodes[i]->name, name) == 0)
			return n->nodes[i];
	}
	
	return NULL;
}


void print_network(Network *n) {
	if(n != NULL) {
		if(strcmp(n->name, "") == 0)
			printf("-> The network is not defined yet\n");
		else
			printf("##  %s  ##\n", n->name);	
		
		if(n->nbNodes > 0) {
			printf(" %d %s\n", n->nbNodes, "node(s):");
			for (int i = 0; i < n->nbNodes; i++)
				printf("  - %s\n",n->nodes[i]->name);
		}
	}
}


void generate_random_network(Network* n, int nbNodes, double averageNmbrLinks, double standardDeviationNmbrLinks, double averageLengthLinks, double standardDeviationLengthLinks) {
	char nodeName[15];
	char linkedNodeName[15];
	for(int i = 0; i < nbNodes; i++) {
		sprintf(nodeName, "random_node_%d", i);
		add_node(n, nodeName);
	}

	int nmbrLinks;
	for(int j = 0; j < nbNodes; j++){
		printf("noeud : %d\n", j);
		sprintf(nodeName, "random_node_%d", j);
		
		nmbrLinks = getTriangularDistributionSample(averageNmbrLinks, standardDeviationNmbrLinks);
		int* linkedNodes = uniqueSetRandomNumber(nmbrLinks, 0, nbNodes - 1, j);
		for(int i = 0; i < nmbrLinks; i++){
			printf("--- lie au noeud : %d\n", *(linkedNodes + i));
			sprintf(linkedNodeName, "random_node_%d", *(linkedNodes + i));
			add_link(get_warehouse(n, nodeName), get_warehouse(n, linkedNodeName), getTriangularDistributionSample(averageLengthLinks, standardDeviationLengthLinks));
		}
		
		free(linkedNodes);
	}
}