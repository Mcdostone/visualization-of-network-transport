#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include "parser.h"
#include "utils.h"


Parser* new_parser(char *filename) {
	Parser *ptr;
	ptr = (Parser *) malloc(sizeof(Parser));
	
	if(ptr == NULL)
		return NULL;

	ptr->filename = (char*) malloc(60 * sizeof(char));
	strcpy(ptr->filename, filename);
	ptr->file = fopen(ptr->filename, "rb");
	ptr->line = (char*) malloc(1024 * sizeof(char));
	*(ptr->line + 1023) = 0;
	ptr->word = NULL;
	ptr->splitted = 1;
	ptr->error = 0;
	ptr->currentLine = 0;
	
	return (ptr->file == NULL) ? NULL : ptr;
}


void free_parser(Parser *p) {
	if(p != NULL) {
		free(p->line);		
		free(p->filename);
		free(p);
	}
}


void next_line(Parser *p) {
	char *line = NULL;
	size_t len = 0;
	char *specialChar;

	if(p->file != NULL) {
		getline(&line, &len, p->file);
		
		if((specialChar = strchr(line, '\n')) != NULL)
			*specialChar = '\0';

		strcpy(p->line, line);
		p->currentLine = p->currentLine + 1;
		p->splitted = 1;
		free(line);
	}
}


void next(Parser *p) {
	if(p->word == NULL || strcmp(p->word, "") == 0)
		next_line(p);

	if(p->splitted == 1) {
		p->word = strtok(p->line, DELIMITER);
		p->splitted = 0;
	}
	else
		p->word = strtok(NULL, DELIMITER);

	if(p->word == NULL)
		next(p);	
}


void check(Parser *p, char *expected, char *regex) {
	if(expected != NULL && p != NULL && p->word != NULL)
		p->error = (strcmp(expected, p->word) == 0) ? 0 : 1;

	if(p != NULL && regex != NULL && expected == NULL && p->word != NULL) {
		regex_t compiledRegex;
		int reti = regcomp(&compiledRegex, regex, REG_EXTENDED);
		
		if(reti)
	    	fprintf(stderr, "Could not compile regex\n");
		else {
			reti = regexec(&compiledRegex, p->word, 0, NULL, 0);
			p->error = reti;
		}
		regfree(&compiledRegex);
	}
}


void error(Parser *p, char *msg, char *expected) {
	if(msg == NULL)
		printf("-> Error at line %d:\nExpected: '%s', actual: '%s'\n", p->currentLine, expected, p->word);
	else {
		printf("-> Error at line %d:\n", p->currentLine);
		printf("%s", msg);
		printf("\n");
	}
}


char* check_header(Parser *p) {
	char *name;
	if(p != NULL && p->error == 0) {		
		next(p);

		if(p->word != NULL) {
			check(p, TYPE_GRAPH, NULL);
			if(p->error == 1)
				error(p, NULL, TYPE_GRAPH);

			next(p);
			check(p, NULL, REGEX_NAME_GRAPH);
			if(p->error == 1)
				error(p, "Incorrect name of graph", NULL);
			else
				name = p->word;
		}
		else
			p->error = 1;
	}

	return name;
}


void check_block(Parser *p, Network *n) {
	if(p != NULL && p->error == 0) {
		
		next(p);
		check(p, "{", NULL);
		if(p->error == 1)
			error(p, NULL, "{");

		next(p);
		check_links(p, n);
		
		check(p, "}", NULL);
		if(p->error == 1)
			error(p, NULL, "}");
	}
}


void check_links(Parser *p, Network *n) {
	if(p!= NULL && p->error == 0) {
		check(p, NULL, REGEX_NODE);
		if(p->error == 0) {
			Warehouse *src;
			Warehouse *dest;
			char *cleanedString;
			
			if(p->error == 0) {
				cleanedString = remove_double_quotes(p->word);
				src = add_node(n, cleanedString);
				free(cleanedString);
			}				
			else
				error(p, "No match with a valid node", NULL);

			next(p);
			check(p,"->", NULL);

			if(p->error == 1) {
				p->error = 0;
				check_links(p, n);
			}
			else {
				next(p);
				check(p, NULL, REGEX_NODE);
				if(p->error == 0) {
					cleanedString = remove_double_quotes(p->word);
					dest = add_node(n, cleanedString);
					free(cleanedString);
				}					
				else
					error(p, "No match with a valid node", NULL);
				
				int tmp;
				next(p);

				tmp = check_properties(p);
				add_link(src, dest, tmp);
				check_links(p, n);
			}
		}
	}
}


int check_properties(Parser *p) {
	int distance = 0;

	check(p, "[", NULL);
	next(p);
	check(p, "label", NULL);
	next(p);
	check(p, "=", NULL);
	next(p);
	check(p, NULL, REGEX_DISTANCE);

	if(p->error == 0) {
		char *cleanedString = remove_double_quotes(p->word);
		distance = atoi(cleanedString);
		free(cleanedString);
	}

	next(p);
	if(strcmp(p->word, ",") == 0) {
		next(p);
		check(p, "color", NULL);
		next(p);
		check(p, "=", NULL);
		next(p);
		check(p, "red", NULL);
		next(p);
	}

	check(p, "]", NULL);
	next(p);

	return distance;
}


void load(Parser *p, Network *n) {
	if(p != NULL && n != NULL) {
		char *name = check_header(p);
		if(p->error == 0) {
			set_name(n, name);
			check_block(p, n);
		}
		close(p);
	}
}


void close(Parser *p) {
	fclose(p->file);
}


void print_parser(Parser *p) {
	if(p != NULL)
		printf("\t%s\n", p->word);
}