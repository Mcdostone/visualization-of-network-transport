#ifndef NETWORK_H
#define NETWORK_H
#include "warehouse.h"


/**
 * A network is composed of a set of warehouses.
 * It's also defined by a name.
 *
 * @author Eliot GODARD
 * @author Yann PRONO
 */
typedef	struct network {
	char name[SIZE_NAME];
	struct warehouse *nodes[MAX_LINKS + 1];
	int nbNodes;
	int hasChanged;
} Network;


/** "Constructor", allocate memory for this structure.
 *
 * @param name Name of the network, used for the DOT file
 * @return Pointor to this new structure
 */
Network* new_network(char *name);


/** 
 * @param n Free the memory allocated for the network n
 */
void free_network(Network *n);


/**
 * Modifies the name of a given network.
 * @param n Network to update
 * @param name New new of the network
 */
void set_name(Network *n, char *name);


/** Add warehouse with direct access to a given warehouse
 *
 * @param n Network 
 * @param name Name of the warehouse
 */
Warehouse* add_node(Network *n, char *name);


/**
 * @param name Name of the warehouse 
 * @return The node warehouse, identified by its name
 */
Warehouse* get_warehouse(Network *n, char *name);


/** Prints all infos about the given warehouse */
void print_network(Network *n);


/** 
 * Generates nodes randomly, based on a ... 
 * @param n Network to use
 * @param nbNodes Number of nodes which will be added
 * @param averageNmbrLinks Number of links for each node on average
 * @param standardDeviation Standard deviation for the number of links
 */
void generate_random_network(Network* n, int nbNodes, double averageNmbrLinks, double standardDeviationNmbrLinks, double averageLengthLinks, double standardDeviationLengthLinks);


#endif