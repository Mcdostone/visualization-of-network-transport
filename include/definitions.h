#define NAME_PGM 		"MANAGE MY WAREHOUSES"
#define VERSION 		"1.0"

#define SIZE_CMD 		20
#define SIZE_NAME 		20
#define COLOR_PATH 		"red"

#define CMD_NEW 		"NEW"
#define NUM_NEW 		"1"

#define CMD_LIST 		"LIST"
#define NUM_LIST 		"2"

#define CMD_DESC 		"DESC"
#define NUM_DESC 		"3"

#define CMD_LINK 		"LINK"
#define NUM_LINK 		"4"

#define CMD_DJIK		"DJIK"
#define NUM_DJIK		"5"

#define CMD_RAND		"RAND"
#define NUM_RAND		"6"

#define CMD_LOAD 		"LOAD"
#define NUM_LOAD 		"7"

#define CMD_SAVE 		"SAVE"
#define NUM_SAVE 		"8"

#define CMD_EXIT 		"EXIT"
#define NUM_EXIT 		"9"

#define MAX_LINKS		100