#ifndef MAIN_H
#define MAIN_H
#include <stdio.h>
#include <stdlib.h>
#include "network.h"


/** Say hello to the user */
void welcome();


/** Prints the menu in the terminal */
void display_menu();


/** asks to the user, the action to trigger */
void get_input_menu(Network *n);


/** Asks the name of the warehouse and returns the pointor this warehouse
 * @param n Network which the warehouse will be added.
 * @return Pointor to the warehouse with the given name.
 */
Warehouse* ask_get_warehouse(Network *n, char *instruction);


/** Defines the name of the network */ 
void define_network(Network *n);


/** All interactions between the user and the programm for creating a new warehouse */
void ask_new_warehouse(Network *n);


/**
 * Asks to the user a new link between two warehouses
 * n Network to update
 */
void ask_create_link(Network *n);


/** Asks to the user a path to follow between 2 warehouses */
Warehouse** ask_djikstra(Network *n, int *lengthPath);


/** Asks all informations to the user to generate random network */
void ask_generate_random_network(Network *n);


/** Load an existing DOT file */
void load_graph(Network *n);


/** Save the current network in a DOT file */
void save_graph(Network *n, Warehouse **path, int lengthPath);


/** Exit the programm */ 
void exit_programm();


#endif