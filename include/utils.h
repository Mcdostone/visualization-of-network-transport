#ifndef UTILS_H
#define UTILS_H
#include <string.h>


/**
 * Converts all of the characters in this String to upper case.
 * @param string String to convert
 */
void to_upper_case(char string[]);


/**
 * Removes the '\n' char after using 'fgets' function.
 */
void remove_newline(char *string);


/**
 * Clears the terminal.
 * Methode bourrin ...
 */
void clear_terminal(void);


/**
 * sanityzes a string (for a filename for example ...).
 *
 * @param string String to sanityze
 * @return sanityzed string
 */
char* sanityze(char string[]);


/**
 * @param nbRandomNumbers Number of integers which will be generated
 * @param minRange 
 * @param maxRange 
 *
 * @return Set of random integers
 */
int* uniqueSetRandomNumber(int nbRandomNumbers, int minRange, int maxRange, int avoidedNumber);


/**
 * @return double for getting triangular distribution
 */
double getTriangularDistributionSample(double expectedValue, double standardDeviation);


/**
 * @return new string without double quotes (used to parse DOT file)
 */
char* remove_double_quotes(char *s);


#endif