#ifndef PARSER_H
#define PARSER_H
#include <stdio.h>
#include "network.h"

#define DELIMITER 			" \t"
#define TYPE_GRAPH 			"digraph"
#define REGEX_NAME_GRAPH 	"[-_[:alnum:]]*"
#define REGEX_NODE 			"[:space:]*\"[-_[:alnum:]]+\"[:space:]*"
#define REGEX_DISTANCE 		"\"[[:digit:]]+\""


typedef	struct parser {
	char *filename;
	int splitted;
	FILE *file;
	char *line;
	char *word;
	int currentLine;
	int error;
} Parser;


/** "Constructor", allocate memory for this structure.
 * Remember, the name must be unique.
 *
 * @param name Pointor on a string, corresponding to the name of the warehouse
 * @return Pointor on the new warehouse
 */
Parser* new_parser(char *filename);


/** Free the memory allocated for the warehouse.
 *
 * @param w The warehouse structure to free
 */
void free_parser(Parser *p);


/** Reads the following line */
void next_line(Parser *p);


/** Get the next word entity in the file */
void next(Parser *p);


/**
 * Check the current word loaded in the parser.
 * @param p current parser
 * @param expected Expected word 
 * @param regex If expected is too precise, you can use a regex
 */
void check(Parser *p, char *expected, char *regex);


/** Prints a error message if there is a problem during parsing */
void error(Parser *p, char *msg, char *expected);


/** Check the header of a DOT file */
char* check_header(Parser *p);


/** Check the content of a block in the current DOT file */
void check_block(Parser *p, Network *n);


/** Check validity of links between nodes */
void check_links(Parser *p, Network *n);


/** Check validity of property for a link 
 * @return The distance described in properties
 */
int check_properties(Parser *p);


/** Main method for loading a file */
void load(Parser *p, Network *n);


/** Close the file in reading */
void close(Parser *p);


/** Informations about status of parser */
void print_parser(Parser *p);


#endif