#ifndef WAREHOUSE_H
#define WAREHOUSE_H
#include "definitions.h"


/**
 * A warehouse is the place where products are stored.
 * It is defined by a unique name and a set of links with others warehouses.
 *
 * @author Eliot GODARD
 * @author Yann PRONO
 */
typedef	struct warehouse {
	char name[SIZE_NAME];
	struct warehouse *links[MAX_LINKS];
	int distances[MAX_LINKS];
	int nbLinks;
} Warehouse;


/** "Constructor", allocate memory for this structure.
 * Remember, the name must be unique.
 *
 * @param name Pointor on a string, corresponding to the name of the warehouse
 * @return Pointor on the new warehouse
 */
Warehouse* new_warehouse(char *name);


/** Free the memory allocated for the warehouse.
 *
 * @param w The warehouse structure to free
 */
void free_warehouse(Warehouse *w);


/** Create link between a source warehouse and a destination warehouse.
 *
 * @param src The source warehouse
 * @param dest The destination warehouse
 * @param distance Distance which separate the two warehouses
 */
void add_link(Warehouse *src, Warehouse *dest, int distance);


/** Prints all infos about the given warehouse */
void print_warehouse(Warehouse *w);


#endif