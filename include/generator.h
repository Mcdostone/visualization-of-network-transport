#ifndef GENERATOR_H
#define GENERATOR_H
#include "network.h"


/**
 * Functions which enable to generate a file 
 * to visualize network with Graphviz.
 *
 * @author Eliot GODARD
 * @author Yann PRONO
 */


/**
 * Main method which create a new DOT file.
 * @param Network to transform in DOT file
 * @return char* the created filename
 */
char* save(Network *n);


/**
 * @param w Array of pointers of warehouses, corresponding to the path to follow
 * @param ref Current warehouse
 * @param lengthPath Size of array w
 * @return Pointer to the next warehouse in order to draw path.
 */
Warehouse* findWarehouse(Warehouse **w, Warehouse *ref, int lengthPath);


/**
 * Calls this method if you want to draw path to follow.
 * The path corresponds to the last executed dkikstra.
 *
 * @param Network to transform in DOT file
 * @param w Array of pointers of warehouses, corresponding to the path to follow
 * @param lengthPath Size of array w
 * @return The name of the created filename
 */
char* saveWithPath(Network *n, Warehouse **w, int lengthPath);


#endif