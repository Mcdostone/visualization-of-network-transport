#ifndef DJIKSTRA_H
#define DJIKSTRA_H
#include "warehouse.h"
#include "network.h"


/**
 * All functions in order
 * to use the Djisktra algorithm
 *
 * @author Eliot GODARD
 * @author Yann PRONO
 */


/**
 * Execute the djikstra algorithm
 * @param n Network to use
 * @param start the source warehouse
 * @param destination the destination warehouse
 * @param *pathLength Number of Nodes 
 * @return Array of Pointer of Warehouses, corresponding to warehouses to follow
 */
Warehouse** djikstra(Network *n, Warehouse *start, Warehouse *destination, int *pathLength);

/**
 *
 */
Warehouse** getPath(int destinationId, int maxIndexThreshold, Warehouse *warehouses[], Warehouse* warehouseBestPath[], int* pathLength);


/** Check if a warehouse isn't already processed */
int isWarehouseUnderThreshold(Warehouse *w, int maxIndexThreshold, Warehouse *warehouses[]);


/** @return the index of the warehouse and of its potential */
int getWarehouseIndex(Warehouse *w, int maxIndexThreshold, Warehouse *warehouses[]);


/**
 *
 */
int getLessPotentialWarehouse(int minIndexThreshold, int maxIndexThreshold, int potentials[], Warehouse *warehouses[]);


#endif