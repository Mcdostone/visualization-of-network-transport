visualization of a network transport
===================

Le but de ce sujet est de simuler un réseau de transport de marchandises. Pour simplifier, on considère une seule marchandise. On dispose d’un certain nombre de dépôts reliés entre eux par des liaisons qui permettent le transport de la marchandise en question. Chaque paire de dépôts ne dispose pas forcément d’une liaison directe. Le programme  à réaliser doit permettre à un utilisateur de :

 - Définir son réseau (le nombre de dépôts, leurs localisation, les liaisons existantes, . . . ). On doit être en mesure de définir la topologie manuellement ou de la générer aléatoirement : on spécifie le nombre de dépôts et on génère aléatoirement leurs positions, le dépôt source, le (les) dépôt(s) destinataire(s), le coût éventuel d’une liaison (cf plus bas).
 - Visualiser le réseau. Il est impose d’utiliser graphviz http://www.graphviz.org/, un logiciel libre de visualisation de graphes.
 - Trouver un chemin qui permet le transport de la marchandise d’un dépôt à un autre avec visualisation graphique de ce chemin

Installation
-------
 
To generate graphs, we have to use Graphviz.
You can install it with:

	sudo apt-get install graphviz


To install the program, you have to execute the following command line:
	
		make build
This will generate the binary program in the 'bin' directory.


How to use
--------
Be sure that the 'main' can be executed:	

	chmod +x ./bin/main
	./bin/main

Once you have generated a DOT file,
you can generate the image associated to this DOT file
by using our shell script: create_image.sh

	create_image.sh my_dot_file.dot


Implemented features
---

All features described in the menu of the program:

	 WELCOME IN 'MANAGE MY WAREHOUSES' 1.0

	A network has been created! You have now the possibility
	to add some warehouses (nodes) to this network.

	 1 - Create a new warehouse (NEW)
	 2 - List of warehouses (LIST)
	 3 - Print informations about a warehouse (DESC)
	 4 - Create a link between warehouses (LINK)
	 5 - Launch Djikstra pathfinding (DJIK)
	 6 - Generate random network (RAND)
	 7 - Load an existing DOT file (LOAD)
	 8 - Save the current network in a file (SAVE)
	 9 - Exit the pr0gramm (EXIT)

When you start the program, a new network is created. This network network is empty  by default.

 - *NEW*: Enable to create a new warehouse which will be added to the current network.
 - *LIST*: Print all known warehouses in the current network.
 - *DESC*: Describe in détails a given warehouse.
 - *LINK*: Create a link between two warehouses. A link is represented by a distance.
 - *DJIK*: Run the Djikstra algorithm in order to find the shortest path between two warehouses.
 - *RAND*: Create a random network, depending of user parameters.
 - *LOAD*: Load a network, represented in a DOT file.
 - *SAVE*: Save the current network in a DOT file.
 - *EXIT*: Exit the programm and save the current network if need it.

Authors
--------

Eliot GODARD, G2

Yann PRONO, G2
